# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 18:11:35 2018

@author: NHATMINH
"""
0.
my_tuple =('a','b','c')
#index 'c':2
# index 'a': 0
#index 'b':1
1.
tup = (1,2,3)
# When trying to change tuple value: TypeError: tuple object does not support item assignment

2. 
my_tuple = (100,200,300,150,250)

3. 
#When trying to sort a tuple, we'll have AttributeError (Tuple does not have the sort attribute)

4.
my_list = [100,110,120,130,140]
my_list.append(150)

5.
#a list is mutuable, meaning you can change and update it.
#whereas a tuple is immutable, meaning you cannot change anything

6.
#In list and tuple, order matters so tuples and lists are considered equal when they have the same items and order

7.
a_tuple =(1,2,3)
list(a_tuple)

8.
#keys() return the key of dictionary
#values() return the value of the keys
#items() return both the keys and values

9.
empty_dic = {}
empty_dic["hello"]="world"
empty_dic["goodbye"]="world"

10.
#it will show the price of APPL

11.
#There are two ways
a_list = [1,2,3]
a_list_2 = list()

a_tuple = (3,4,5)
a_tuple_2 = tuple()

a_dict = {'a':1,'b':2}
a_dict_2 = dict()

12.
print(a_dict.pop('a'))
del a_dict['b']
print(a_dict)
#pop will show the value of the key, del does not

13.
def a_list_append(a_list):
    a_list.append("homie")
    return a_list

print(a_list_append(["hello","my love"]))

14.
GOOG = 100
APPL = 200
KPMG = 150
BCG = 50
OSYS = 250
PHZR = 180
average = (GOOG + APPL + KPMG + BCG + OSYS + PHZR)/7
print(average)

def add_up(list_argument):
    average = sum(list_argument)/7
    return average

print(add_up([100,200,150,50,250,180]))