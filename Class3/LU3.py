# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 15:25:17 2018

@author: NHATMINH
"""

1.
#a scope is the applicability of a variables
#a variable has a local scope when it can only be used within that specific function
#a variable has a global scope when it is available globally

2.
#first_var : global scope
#who : local scope - hello_world scope
#second_var: global scope
#who_again: local scope - hello_other_world scope

3. 
#There is an error. Even though first_var is a global cope, it is only initialized after the print statement

4.
a= 10
def function():
    b = a*2
    return b
c = function()
print(c)
print(a)

5.
#There will be an error since _power_ is a generate_power scope.
# if print(_power_) is removed, print(a) wil show 2**10

6.
#Before and after the function, print(power) will be the same since it is not affected by generate_power local scope.

7.
#The prints will be different due as the function transaction has a different scope and the print statement has a different scope

9. 
def keyword_argument(first_name='Minh',last_name='Hoang'):
    return(first_name,last_name)

keyword_argument()
keyword_argument(first_name='Joao',last_name='Hoang')
keyword_argument('Steve','Jobs')

10.
first_name='Ricardo'
last_name='Pereira'
def name(first_name='Sam',last_name='Hopskin'):
    print(first_name,last_name)
    
#Without argument, the function name would use the default arguments
#when assigned first_name=first_name, the function would use the global scope 
#when redefined the keyword argument the function would use the arguments given

11.
def greetings(message='Good morning',name=''):
    print(message,name)


    


