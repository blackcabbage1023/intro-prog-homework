# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:38:37 2018

@author: NHATMINH
"""

1.
def power(a,b):
    return a**b

2.
def equal_function(x,y):
    return x == y


3.
def hypotenuse(a,b):
    return (a**2 + b**2)**(1/2)

4.
def litecoin_convert(bitcoin):
    litecoin = 58*bitcoin
    return litecoin
def ethereum_convert(bitcoin):
    ethereum = 17*bitcoin
    return ethereum
def euro_convert(bitcoin):
    euro = 5741*bitcoin
    return euro

5.
#When a function does not have the return function, it does not return anything. As such, when print you'll see None and its type is NonType

6.
#A function that prints the value will show the value on the screen. On the otherhand, the function that returns the value only contains the result. You'll need to call it so that the result shows up.

7.
#REPL shows OUT when the statement is not null. For example, if you define a = 3, then you call a. REPL will show OUT = 3 since a has a value. In the case of print(a), REPL will not show OUT because the print function is NULL. To verify it, we use type(print(a)), and the result is NoneType

