1. 
print(bool(1))
print(bool(1.1))
print(bool(0))
print(bool(0.0001))
print(bool(""))
print(bool(None))

2.
def function_2(ticker):
    if ticker == "APPL":
        return True
    else:
        return False
    
3.
def function_3(gender):
    if gender == "male":
        return "You like pink"
    if gender == "female":
        return "You hate pink"

4.
def gender(gender):
    if gender == "male":
        print('You hate pink')
        
    if gender == "female":
        print("You like pink")
        print("This line should not be printed")

5.
def function_5(mom_age,dad_age):
    if mom_age == dad_age:
        return mom_age + dad_age
    else:
        return dad_age - mom_age
    
6.
def function_6(a_dict,a_list):
    if len(a_dict) == len(a_list):
        return "awesome"
    else:
        return "that is sad"

7.
stock_dict = {'APPL':100,'GOOGL':200,'FB':150}
for key in stock_dict:
    print(key)
for key in stock_dict:
    print(stock_dict[key])
for key in stock_dict:
    print('the stock price of',key,'is',stock_dict[key])
    
8.
my_tuple = (10,20,30)
my_list = [40,50,60]
my_dict = {'APPL':100,'GOOG':90,'FB':80}

for num in my_tuple:
    print(num)
for num in my_list:
    print(num)
for key in my_dict:
    print(key,my_dict[key])

9.
new_list = []
def function_9(a_list):
    for num in a_list:
        new_list.append(num*2)
    return new_list

10.
new_dict = {}
def function_10(a_dict):
    for key in a_dict:
        new_dict[key]=a_dict[key]*2
    return new_dict

11.
def function_11(a_list,a_number):
    if len(a_list) > a_number:
        return True
    
    
    
    
    